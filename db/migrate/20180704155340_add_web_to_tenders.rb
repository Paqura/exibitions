class AddWebToTenders < ActiveRecord::Migration[5.2]
  def change
    add_column :tenders, :web, :string
  end
end
