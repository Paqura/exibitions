class AddCompanyInfoToTenders < ActiveRecord::Migration[5.2]
  def change
    add_column :tenders, :company_info, :text
  end
end
