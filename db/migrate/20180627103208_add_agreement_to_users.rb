class AddAgreementToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :agreement, :boolean, default: true
  end
end
