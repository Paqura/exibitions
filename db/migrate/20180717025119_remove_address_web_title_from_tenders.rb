class RemoveAddressWebTitleFromTenders < ActiveRecord::Migration[5.2]
  def change
    remove_column :tenders, :address, :string
    remove_column :tenders, :web, :string
    remove_column :tenders, :title, :string
  end
end
