class AddPublicToTenders < ActiveRecord::Migration[5.2]
  def change
    add_column :tenders, :public, :boolean, default: false
  end
end
