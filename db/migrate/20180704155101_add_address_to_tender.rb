class AddAddressToTender < ActiveRecord::Migration[5.2]
  def change
    add_column :tenders, :address, :string
  end
end
