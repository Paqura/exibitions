class UpdateTendersForeignKey < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key "tenders", "users"

    add_foreign_key "tenders", "users"
  end
end
