class AddMainPurposeParticipationToTenders < ActiveRecord::Migration[5.2]
  def change
    add_column :tenders, :main_purpose_participation, :text
  end
end
