class InvitationMailer < ApplicationMailer
  def invitation
    @user = params[:user]
    @email = params[:email]
    @url = user_invatee_url(@user)
    mail(to: @email, subject: 'Приглашаем участвовать в тендере')
  end
end
