if @comment.ufile?
  @filename = link_to @comment.ufile.file.filename, @comment.ufile.url, target: '_blank'
  puts @filename
else
  nil
end

json.set! :html_content,

    append: {
        "#comments#{@comment.review_id}": "<h6><em>#{@comment.user.name}:</em></h6>
        <p class='comment-body'>#{@comment.body}
          <br />
          файл: #{@filename}
        </p>"
    },

    errors: {
        "#comment-errors#{@comment.review_id}": "#{@comment.errors.full_messages.join("\n")}"
    }
