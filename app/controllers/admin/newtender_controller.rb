class Admin::NewtenderController < ApplicationController
  before_action :authenticate_admin!
  before_action :load_model, only: %i[edit update destroy]
  layout 'admin'

  def new
    @tender = Tender.new
  end

  def create
    @tender = Tender.new tender_params
    if @tender.save
      redirect_to admin_tenders_path, success: 'Тендер успешно создан'
    else
      flash[:danger] = 'Не удалось создать тендер'
      render :new
    end
  end

  def edit; end

  def update
    if @tender.update tender_params
      redirect_to admin_tenders_path, success: 'Тендер успешно обновлен'
    else
      flash[:danger] = 'Не удалось обновить тендер'
      render :edit
    end
  end

  def destroy
    @tender.destroy
    redirect_to admin_tenders_path, success: 'Тендер удален'
  end

  private

    def tender_params
      params.require(:tender).permit(
        :address, :title, :budget, :floor, :region, :exhibition, :offer_counter, :obligatory_condition,
          :message_count, :technical_specification, :company_info, :main_purpose_participation, :web,
          :main_focus, :special_request, :lighting, :choice_materials, :electrical, :utility_room,
          :negotiation_zone, :additional_equipment, :showcases, :reception, :exhibited_products,
          :height_stand, :open_to_visitors, :image_saturation, :main_focus_stand_more, :area, :company_info,
          :tender_number_participants, :term_delivery_design_project, :dates_event, :web, :user_id,
          :main_purpose_participation
      )
    end

    def load_model
      @tender = Tender.find params[:id]
    end
end
