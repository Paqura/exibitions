class Admin::TendersController < ApplicationController
  before_action :authenticate_admin!
  before_action :load_model, only: %i[show edit update destroy public_on]
  layout 'admin'

  def index
    if params[:user_id]
      @user = User.find(params[:user_id])
      @tenders = @user.tenders.order('created_at DESC')
      @one_user = true
    else
      if params[:order_users].blank? && params[:order_htitle].blank?
        @tenders = Tender.order('created_at DESC')
      else
        @tenders = Tender.by_user(params[:order_users]) if params[:order_users]
        @tenders = Tender.by_title(params[:order_htitle]) if params[:order_htitle]
      end
    end
  end

  def show; end

  def new
    @user = User.find(params[:user_id])
    session[:user_id] = @user.id
    @tender = Tender.new
  end

  def create
    @user = User.find(session[:user_id])
    @tender = @user.tenders.build tender_params
    if @tender.save
      redirect_to admin_tenders_path, success: 'Тендер успешно создан'
    else
      flash[:danger] = 'Не удалось создать тендер'
      render :new
    end
  end

  def edit; end

  def update
    if @tender.update tender_params
      redirect_to admin_tenders_path, success: 'Тендер успешно обновлен'
    else
      flash[:danger] = 'Не удалось обновить тендер'
      render :edit
    end
  end

  def destroy
    @tender.destroy
    redirect_to admin_tenders_path, success: 'Тендер удален'
  end

  def public_on
    if @tender.update(public: true)
      @url = tender_url(@tender)
      NewTenderMailer.new_tender(@url, @tender).deliver_later
      redirect_to admin_tenders_path, success: 'Тендер успешно опубликован'
    else
      flash[:danger] = 'Не удалось опубликовать тендер'
      render admin_tenders_path
    end
  end

  private

    def tender_params
      params.require(:tender).permit(
        :budget, :floor, :region, :exhibition, :offer_counter, :obligatory_condition, :company_info,
        :message_count, :technical_specification, :company_info, :main_purpose_participation,
        :main_focus, :special_request, :lighting, :choice_materials, :electrical, :utility_room,
        :negotiation_zone, :additional_equipment, :showcases, :reception, :exhibited_products,
        :height_stand, :open_to_visitors, :image_saturation, :main_focus_stand_more, :area,
        :tender_number_participants, :term_delivery_design_project, :dates_event, :user_id
      )
    end

    def load_model
      @tender = Tender.find params[:id]
    end
end
