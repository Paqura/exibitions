class Admin::UsersController < ApplicationController
  before_action :authenticate_admin!
  before_action :load_model, only: %i[show edit update destroy]
  layout 'admin'

  def index
    @users = User.all
    @partners = @users.partners
    @users = @users.clients
  end

  def show; end

  def new
    @user = User.new
  end

  def create
    @user = User.new user_params
    if @user.save
      redirect_to admin_users_path, success: 'пользователь успешно создан'
    else
      flash[:danger] = 'Не удалось создать пользователя'
      render :new
    end
  end

  def edit; end

  def update
    if @user.update user_params
      redirect_to admin_users_path, success: 'пользователь успешно обновлен'
    else
      flash[:danger] = 'Не удалось обновить пользователя'
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path, success: 'пользователь удален'
  end

  private

    def user_params
      params.require(:user).permit(
        :email, :password, :password_confirmation, :purpose, :partner, :name, :phone, :company, :agreement
      )
    end

    def load_model
      @user = User.find params[:id]
    end
end
