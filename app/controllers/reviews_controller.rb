class ReviewsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_tender
  before_action :partner?
  before_action :find_review, only: [:edit, :update, :destroy]


  def new
    redirect_to @tender if current_user.reviews.pluck(:tender_id).include?(@tender.id)
    @review = Review.new
  end

  def create
    @review = current_user.reviews.build(review_params)
    @review.tender_id = @tender.id

    if @review.save
      redirect_to tender_path(@tender), success: 'Ваше предложение успешно сохранено'
    else
      flash[:danger] = 'Не удалось сохранить предложение'
      render 'new'
    end
  end

  def edit
  end

  def update
    if @review.update(review_params)
      redirect_to tender_path(@tender), success: 'Ваше предложение успешно обновлено'
    else
      flash[:danger] = 'Не удалось обновить предложение'
      render 'edit'
    end
  end

  def destroy
    @review.destroy
    redirect_to tender_path(@tender)
  end

  private

    def review_params
      params.require(:review).permit(:comment, attachments: [])
    end

    def find_tender
      @tender = Tender.find(params[:tender_id])
    end

    def find_review
      @review = Review.find(params[:id])
    end

    def partner?
      redirect_to @tender unless current_user.partner?
    end
end
