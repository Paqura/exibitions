class UsersCabinetController < ApplicationController
  before_action :authenticate_user!
  def index
    @user = current_user
    @tenders = @user.partner? ? Tender.all : @user.tenders.all
  end
end
