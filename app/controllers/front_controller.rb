class FrontController < ApplicationController
  def index
    redirect_to tenders_path if user_signed_in?
    redirect_to admin_tenders_path if admin_signed_in?
    if params[:order_users].blank? && params[:order_htitle].blank?
      @tenders = Tender.order('created_at DESC')
    else
      @tenders = Tender.by_user(params[:order_users]) if params[:order_users]
      @tenders = Tender.by_title(params[:order_htitle]) if params[:order_htitle]
    end
  end
end
