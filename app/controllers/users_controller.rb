class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    @users = current_user.partner ? User.clients : User.partners
  end

  def show
    @user = User.find(params[:id])
  end
end
