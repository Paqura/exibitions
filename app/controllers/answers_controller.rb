class AnswersController < ApplicationController
  def create
    @question = Question.find(params[:question_id])
    @answer = @question.answers.build(answer_params)
    @answer.user_id = current_user.id
    @answer.save
    render layout: false
  end

  private

  def answer_params
    params.require(:answer).permit(:body, :ufile)
  end
end
