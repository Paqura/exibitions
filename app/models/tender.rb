class Tender < ApplicationRecord
  belongs_to :user
  has_many :reviews, dependent: :destroy
  has_many :questions, dependent: :destroy

  scope :by_user, -> (name) { joins('INNER JOIN users ON tenders.user_id = users.id').order("#{name}") }
  scope :by_title, -> (htitle) { order("#{htitle} DESC") }

  class << self
    def main_purpose
      ['Поддержание имиджа (констатация присутствия, лидерства)',
       'Презентация новинок/стимулирование продаж.',
       'Поддержание партнерских отношений и расширение бизнес-контактов']
    end

    def mainFocus
      ['Логотип/слоган',
       'Продукт/новинку',
       'Ключевое изображение',
       'Концепт-объект (инсталляция, макет, декорация)',
       'Презентационная зона. Зона шоу. Игровая зона']
    end

    def imageSaturation
      ['Максимально',
       'Умеренно',
       'Минимально']
    end

    def openToVisitors
      ['Максимально открытым и просматриваемым',
       'Хорошо просматриваемым с ограниченной зоной доступа',
       'Закрытым']
    end
  end
end
