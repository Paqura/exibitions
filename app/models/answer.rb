class Answer < ApplicationRecord
  belongs_to :user
  belongs_to :question

  mount_uploader :ufile, UfileUploader

  validates :body, presence: true
end
