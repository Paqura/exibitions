FactoryBot.define do
  sequence :email do |n|
    "user#{n}@test.com"
  end

  factory :user do
    email
    password '12345678'
    password_confirmation '12345678'
    name 'a'
    phone '111111'
    company 'A'
    agreement true
    purpose 'purpose'
  end
end
