require 'rails_helper'

RSpec.feature 'Tenders', type: :feature do
  given(:user) { create(:user) }

  before do
    visit new_user_session_path
    sign_in(user)
  end

  scenario 'При входе клиент видит кнопку Тендеры' do
    expect(page).to have_link 'Тендеры'
  end

  context 'Переход к созданию тендера' do

    scenario 'Кликая на кнопку "тендеры", переходим на страницу "тендеры", где имеется заголовок "Мои тендеры" и кнопка "Создать тендер"' do
      click_on 'Тендеры'

      expect(current_path).to eq tenders_path
      expect(page).to have_link 'Техническое задание'
    end

    scenario 'Кликая на кнопку "Создать тендер" переходим на страницу создания тендера с заголовком "Техническое задание"' do
      visit tenders_path
      click_on 'Техническое задание'
      expect(current_path).to eq new_tender_path
      expect(page).to have_content 'Техническое задание'
    end
  end

  context 'заполнение формы технического задания' do
    before do
      visit new_tender_path
    end

    scenario 'Выставка' do
      fill_in 'Выставка', with: 'Лошадей'
      click_on 'Сохранить'

      expect(user.tenders.last.exhibition).to eq 'Лошадей'
    end

    scenario 'Выставка, проверка на ошибку' do
      fill_in 'Выставка', with: 'Лошадей'
      click_on 'Сохранить'

      expect(user.tenders.last.exhibition).to_not eq 'Коней'
    end

    scenario 'Место проведения' do
      fill_in 'Место проведения', with: 'Москва'
      click_on 'Сохранить'

      expect(user.tenders.last.region).to eq 'Москва'
    end

    scenario 'Место проведения, проверка на ошибку' do
      fill_in 'Место проведения', with: 'Москва'
      click_on 'Сохранить'

      expect(user.tenders.last.region).to_not eq 'Моква'
    end

    scenario 'Сроки проведения' do
      fill_in 'Сроки проведения', with: '23-30'
      click_on 'Сохранить'

      expect(user.tenders.last.dates_event).to eq '23-30'
    end

    scenario 'Сроки проведения, проверка на ошибку' do
      fill_in 'Сроки проведения', with: '23-30'
      click_on 'Сохранить'

      expect(user.tenders.last.dates_event).to_not eq '23-31'
    end

    scenario 'Бюджет' do
      fill_in 'Бюджет(руб)', with: 230000
      click_on 'Сохранить'

      expect(user.tenders.last.budget).to eq 230000
    end


    scenario 'Бюджет, проверка на ошибку' do
      fill_in 'Бюджет(руб)', with: 230000
      click_on 'Сохранить'

      expect(user.tenders.last.budget).to_not eq 230002
    end

    scenario 'Тендер' do
      fill_in 'Тендер (число участников)', with: 23
      click_on 'Сохранить'

      expect(user.tenders.last.tender_number_participants).to eq 23
    end

    scenario 'Тендер, проверка на ошибку' do
      fill_in 'Тендер (число участников)', with: 23
      click_on 'Сохранить'

      expect(user.tenders.last.tender_number_participants).to_not eq 24
    end

    scenario 'Площадь стенда' do
      fill_in 'Площадь стенда', with: 2334
      click_on 'Сохранить'

      expect(user.tenders.last.area).to eq 2334
    end

    scenario 'Площадь стенда, проверка на ошибку' do
      fill_in 'Площадь стенда', with: 2334
      click_on 'Сохранить'

      expect(user.tenders.last.area).to_not eq 234
    end

    scenario 'Этажность' do
      fill_in 'Этажность', with: 2
      click_on 'Сохранить'

      expect(user.tenders.last.floor).to eq 2
    end

    scenario 'Этажность, проверка на ошибку' do
      fill_in 'Этажность', with: 2
      click_on 'Сохранить'

      expect(user.tenders.last.floor).to_not eq 3
    end

    scenario 'Зал: Павильон:' do
      fill_in 'Зал: Павильон:', with: 'Zal'
      click_on 'Сохранить'

      expect(user.tenders.last.company_info).to eq 'Zal'
    end

    scenario 'Зал: Павильон:, проверка на ошибку' do
      fill_in 'Зал: Павильон:', with: 'Zal'
      click_on 'Сохранить'

      expect(user.tenders.last.company_info).to_not eq 'Павильон'
    end

    scenario 'Основная цель участия в выставке' do
      select 'Поддержание партнерских отношений и расширение бизнес-контактов', from: 'Основная цель участия в выставке'
      click_on 'Сохранить'

      expect(user.tenders.last.main_purpose_participation).to eq 'Поддержание партнерских отношений и расширение бизнес-контактов'
    end

    scenario 'Основная цель участия в выставке, проверка на ошибку' do
      select 'Поддержание партнерских отношений и расширение бизнес-контактов', from: 'Основная цель участия в выставке'
      click_on 'Сохранить'

      expect(user.tenders.last.main_purpose_participation).to_not eq 'Презентация новинок/стимулирование продаж.'
    end

    scenario 'что должен увидеть Ваш посетитель в первую очередь:' do
      select 'Продукт/новинку', from: 'что должен увидеть Ваш посетитель в первую очередь:'
      click_on 'Сохранить'

      expect(user.tenders.last.main_focus).to eq 'Продукт/новинку'
    end

    scenario 'что должен увидеть Ваш посетитель в первую очередь:, проверка на ошибку' do
      select 'Продукт/новинку', from: 'что должен увидеть Ваш посетитель в первую очередь:'
      click_on 'Сохранить'

      expect(user.tenders.last.main_focus).to_not eq 'Логотип/слоган'
    end

    scenario 'Ассоциации, которые должен вызывать выставочный стенд:' do
      fill_in '(Например, яркий, динамичный и т.п.)', with: 'динамичный'
      click_on 'Сохранить'

      expect(user.tenders.last.main_focus_stand_more).to eq 'динамичный'
    end

    scenario 'Ассоциации, которые должен вызывать выставочный стенд:, проверка на ошибку' do
      fill_in '(Например, яркий, динамичный и т.п.)', with: 'динамичный'
      click_on 'Сохранить'

      expect(user.tenders.last.main_focus_stand_more).to_not eq 'яркий'
    end

    scenario 'В каком стиле Вы хотите видеть стенд:' do
      select 'Максимально', from: '(имиджевыми/техническими):'
      click_on 'Сохранить'

      expect(user.tenders.last.image_saturation).to eq 'Максимально'
    end

    scenario 'В каком стиле Вы хотите видеть стенд:, проверка на ошибку' do
      select 'Максимально', from: '(имиджевыми/техническими):'
      click_on 'Сохранить'

      expect(user.tenders.last.image_saturation).to_not eq 'Умеренно'
    end

    scenario 'Насколько стенд должен быть открытым для посетителей:' do
      select 'Максимально открытым и просматриваемым', from: 'Насколько стенд должен быть открытым для посетителей:'
      click_on 'Сохранить'

      expect(user.tenders.last.open_to_visitors).to eq 'Максимально открытым и просматриваемым'
    end

    scenario 'Насколько стенд должен быть открытым для посетителей:, проверка на ошибку' do
      select 'Максимально открытым и просматриваемым', from: 'Насколько стенд должен быть открытым для посетителей:'
      click_on 'Сохранить'

      expect(user.tenders.last.open_to_visitors).to_not eq 'Хорошо просматриваемым с ограниченной зоной доступа'
    end

    scenario 'Высота стенда: (Предпочтения/ограничения по высоте стенда)' do
      fill_in 'Высота стенда: (Предпочтения/ограничения по высоте стенда)', with: '6m'
      click_on 'Сохранить'

      expect(user.tenders.last.height_stand).to eq '6m'
    end

    scenario 'Высота стенда: (Предпочтения/ограничения по высоте стенда), проверка на ошибку' do
      fill_in 'Высота стенда: (Предпочтения/ограничения по высоте стенда)', with: '5m'
      click_on 'Сохранить'

      expect(user.tenders.last.height_stand).to_not eq '6m'
    end

    scenario 'Зона ресепшн:' do
      fill_in 'Зона ресепшн:', with: '6 стоек'
      click_on 'Сохранить'

      expect(user.tenders.last.reception).to eq '6 стоек'
    end

    scenario 'Зона ресепшн:, проверка на ошибку' do
      fill_in 'Зона ресепшн:', with: '6 стоек'
      click_on 'Сохранить'

      expect(user.tenders.last.reception).to_not eq '6 стульев'
    end

    scenario 'Экспонируемая продукция:' do
      fill_in 'вписать', with: '6 стоек'
      click_on 'Сохранить'

      expect(user.tenders.last.exhibited_products).to eq '6 стоек'
    end

    scenario 'Экспонируемая продукция:, проверка на ошибку' do
      fill_in 'вписать', with: '6 стоек'
      click_on 'Сохранить'

      expect(user.tenders.last.exhibited_products).to_not eq '5 стоек'
    end

    scenario 'Наличие витрин (количество, размеры):' do
      fill_in 'Наличие витрин (количество, размеры):', with: '6x4'
      click_on 'Сохранить'

      expect(user.tenders.last.showcases).to eq '6x4'
    end

    scenario 'Наличие витрин (количество, размеры):, проверка на ошибку' do
      fill_in 'Наличие витрин (количество, размеры):', with: '6x4'
      click_on 'Сохранить'

      expect(user.tenders.last.showcases).to_not eq '6x5'
    end

    scenario 'Оборудование для экспонируемой продукции:' do
      fill_in 'дополнительное оборудование', with: 'Проекционные экраны.'
      click_on 'Сохранить'

      expect(user.tenders.last.additional_equipment).to eq 'Проекционные экраны.'
    end

    scenario 'Оборудование для экспонируемой продукции:, проверка на ошибку' do
        fill_in 'дополнительное оборудование', with: 'Проекционные экраны.'
        click_on 'Сохранить'

        expect(user.tenders.last.additional_equipment).to_not eq 'Проекционные экраны. 6 штук '
      end

    scenario 'Переговорные зоны:' do
      fill_in 'Открытая. Закрытая. Площадь. Оснащение мебелью, на сколько человек рассчитано.Взаиморасположение с другими зонами.', with: 'Оснащение мебелью'
      click_on 'Сохранить'

      expect(user.tenders.last.negotiation_zone).to eq 'Оснащение мебелью'
    end

    scenario 'Переговорные зоны:, проверка на ошибку' do
      fill_in 'Открытая. Закрытая. Площадь. Оснащение мебелью, на сколько человек рассчитано.Взаиморасположение с другими зонами.', with: 'Оснащение мебелью'
      click_on 'Сохранить'

      expect(user.tenders.last.negotiation_zone).to_not eq 'Оснащение мебелью и душем'
    end

    scenario 'Кухня. Раздевалка. Склад:' do
      fill_in 'Кухня. Раздевалка. Склад. Площадь. Оснащение (стеллаж, вешалка, мойка, кофемашина, холодильник, кулер, корзина).', with: 'стеллаж, вешалка, мойка, кофемашина, холодильник, кулер, корзина'
      click_on 'Сохранить'

      expect(user.tenders.last.utility_room).to eq 'стеллаж, вешалка, мойка, кофемашина, холодильник, кулер, корзина'
    end

    scenario 'Кухня. Раздевалка. Склад:, проверка на ошибку' do
      fill_in 'Кухня. Раздевалка. Склад. Площадь. Оснащение (стеллаж, вешалка, мойка, кофемашина, холодильник, кулер, корзина).', with: 'стеллаж, вешалка, мойка, кофемашина, холодильник, кулер, корзина'
      click_on 'Сохранить'

      expect(user.tenders.last.utility_room).to_not eq 'вешалка, мойка, кофемашина, холодильник, кулер, корзина'
    end

    scenario 'Экспонаты, требующие подключения, количество розеток:' do
      fill_in 'Экспонаты, требующие подключения, количество розеток:', with: 'мойка, кофемашина, холодильник, кулер'
      click_on 'Сохранить'

      expect(user.tenders.last.electrical).to eq 'мойка, кофемашина, холодильник, кулер'
    end

    scenario 'Экспонаты, требующие подключения, количество розеток:, проверка на ошибку' do
      fill_in 'Экспонаты, требующие подключения, количество розеток:', with: 'мойка, кофемашина, холодильник, кулер'
      click_on 'Сохранить'

      expect(user.tenders.last.electrical).to_not eq 'мойка, офемашина, холодильник, кулер'
    end

    scenario 'Предпочтения в выборе используемых материалов:' do
      fill_in 'Предпочтения в выборе используемых материалов:', with: 'мойка, кофемашина, холодильник, кулер'
      click_on 'Сохранить'

      expect(user.tenders.last.choice_materials).to eq 'мойка, кофемашина, холодильник, кулер'
    end

    scenario 'Предпочтения в выборе используемых материалов:, проверка на ошибку' do
      fill_in 'Предпочтения в выборе используемых материалов:', with: 'мойка, кофемашина, холодильник, кулер'
      click_on 'Сохранить'

      expect(user.tenders.last.choice_materials).to_not eq 'кофемашина, холодильник, кулер'
    end

    scenario 'Освещение стенда:' do
      fill_in 'Освещение стенда:', with: 'Лампочка Ильича'
      click_on 'Сохранить'

      expect(user.tenders.last.lighting).to eq 'Лампочка Ильича'
    end

    scenario 'Освещение стенда:, проверка на ошибку' do
      fill_in 'Освещение стенда:', with: 'Лампочка Ильича'
      click_on 'Сохранить'

      expect(user.tenders.last.lighting).to_not eq 'Лампочка И '
    end

    scenario 'Обязательные условия' do
      fill_in 'Например: Собственное производство у компании', with: 'Собственное производство у компании'
      click_on 'Сохранить'

      expect(user.tenders.last.obligatory_condition).to eq 'Собственное производство у компании'
    end

    scenario 'Обязательные условия, проверка на ошибку' do
      fill_in 'Например: Собственное производство у компании', with: 'Собственное производство у компании'
      click_on 'Сохранить'

      expect(user.tenders.last.obligatory_condition).to_not eq 'Собственное производство '
    end

    scenario 'Общие данные технического задания' do
      fill_in 'Общие данные технического задания', with: 'Общие данные технического задания'
      click_on 'Сохранить'

      expect(user.tenders.last.technical_specification).to eq 'Общие данные технического задания'
    end

    scenario 'Общие данные технического задания, проверка на ошибку' do
      fill_in 'Общие данные технического задания', with: 'Общие данные технического задания'
      click_on 'Сохранить'

      expect(user.tenders.last.technical_specification).to_not eq 'Общие данные '
    end

    scenario 'Особые пожелания к стенду' do
      fill_in 'Особые пожелания к стенду', with: 'Особые пожелания к стенду'
      click_on 'Сохранить'

      expect(user.tenders.last.special_request).to eq 'Особые пожелания к стенду'
    end

    scenario 'Особые пожелания к стенду, проверка на ошибку' do
      fill_in 'Особые пожелания к стенду', with: 'Особые пожелания к стенду'
      click_on 'Сохранить'

      expect(user.tenders.last.special_request).to_not eq 'Особые пожелания к стенду я'
    end
  end
end
