Rails.application.routes.draw do
  resources :answers
  mount Ckeditor::Engine => '/ckeditor'

  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }
  devise_for :admins, controllers: { sessions: 'admins/sessions', registrations: 'admins/registrations' }

  get 'choice_client_partner/choice_page'
  post 'choice_client_partner/create'
  get 'kso/index'
  resources :profiles, except: [:index]
  get 'profiles/:id/invite' => 'profiles#invite', as: :invite

  get 'cabinet', to: 'users_cabinet#index', as: :cabinet

  resources :tenders do
    resources :reviews do
      resources :comments, only: [:create, :edit]
    end

    resources :questions do
      resources :answers, only: [:create, :edit]
    end
  end

  resources :users, only: [:index, :show] do
    get 'invatee' => 'tenders#invatee', as: :invatee
  end

  namespace :admin do
    get '', to: 'dashboards#show', as: :admin
    get 'profile/:id', to: 'profiles#show', as: :user_profile

    resources :tenders do
      member do
        put 'public_on', to: 'tenders#public_on'
        put 'public_off', to: 'tenders#public_off'
      end

      resources :reviews
    end

    resources :newtender

    resources :users do
      resources :tenders
    end
  end

  root 'front#index'
end
